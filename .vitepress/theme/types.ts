export interface ThemeConfig {
  version: string;
  gitlabRepo: string;
}
