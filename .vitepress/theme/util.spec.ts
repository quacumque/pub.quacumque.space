import { expect, test, describe } from "vitest";
import { type TitlePart, cutTitleParts } from "./util";

describe("cutting title parts", () => {
  function createSample(): TitlePart[] {
    return [
      { text: "foo", color: "black" },
      { text: "bar", color: "dimmed" },
    ];
  }

  test("when limit is max", () => {
    const result = cutTitleParts(createSample(), 6);

    expect(result).toMatchInlineSnapshot(`
          [
            {
              "color": "black",
              "text": "foo",
            },
            {
              "color": "dimmed",
              "text": "bar",
            },
          ]
        `);
  });

  test("when limit is max - 1", () => {
    const result = cutTitleParts(createSample(), 5);

    expect(result).toEqual([
      { text: "foo", color: "black" },
      { text: "ba", color: "dimmed" },
    ] satisfies TitlePart[]);
  });

  test("when limit is 2", () => {
    const result = cutTitleParts(createSample(), 2);

    expect(result).toEqual([
      { text: "fo", color: "black" },
    ] satisfies TitlePart[]);
  });

});
