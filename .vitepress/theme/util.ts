export type TitlePart = { text: string; color: "black" | "dimmed" };

export function cutTitleParts(
  source: TitlePart[],
  maxLength: number
): TitlePart[] {
  const parts: TitlePart[] = [];
  let len = 0;
  for (const item of source) {
    if (len + item.text.length <= maxLength) {
      parts.push(item);
      len += item.text.length;
    } else {
      const remaining = maxLength - len;
      const cut = item.text.slice(0, remaining);
      parts.push({ text: cut, color: item.color });
      break;
    }
  }

  return parts;
}
