import {  defineConfigWithTheme } from "vitepress";
import Icons from 'unplugin-icons/vite'
import type { ThemeConfig } from './theme/types'

export default defineConfigWithTheme<ThemeConfig>({
  title: "pub.quacumque.space",
  description: "Yet Another personal site of a Yet Another programmer.",
  srcDir: "src",
  markdown: {
    theme: 'github-dark-dimmed',
  },
  vite: {
    plugins: [Icons()],
    // envDir: '../'
  },
  themeConfig: {
    version: '0.1.0',
    gitlabRepo: 'https://gitlab.com/quacumque/pub.quacumque.space'
  }
});
