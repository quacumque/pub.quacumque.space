# pub.quacumque.space

The sources of https://pub.quacumque.space.

## Development

**Prepare:**

1. Install Node.js v16.9+ (or whatever supports Corepack)
2. `corepack enable`
3. `pnpm install`

**Run in dev mode:**

```bash
pnpm dev
```

**Build for distribution:** (output is in `.vitepress/dist`)

```bash
pnpm build
```
