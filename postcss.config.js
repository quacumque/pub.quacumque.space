import tailwind from "tailwindcss";

export default {
  plugins: [
    tailwind({
      content: ["./.vitepress/theme/**/*.vue"],
      theme: {
        extend: {
          colors: {
            'vp-brand': {
              DEFAULT: "var(--vp-c-brand)",
              light: "var(--vp-c-brand-light)",
              lighter: "var(--vp-c-brand-lighter)",
              dark: "var(--vp-c-brand-dark)",
              darker: "var(--vp-c-brand-darker)",
            },
          },
          backgroundColor: {
            'vp': {
              DEFAULT: 'var(--vp-c-bg)'
            }
          }
        },
        fontFamily: {
          sans: ["var(--font-sans)", "sans-serif"],
          serif: ["var(--font-serif)", "serif"],
          mono: ["var(--font-monospace)", "monospace"],
        },
      },
    }),
  ],
};
